public class Food
{
    private String description;
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String inDescription)
    {
        this.description = inDescription;
    }
   private int calories;
    
    public int getCalories()
    {
        return calories;
    }
    
    public void setCalories(int inCalories)
    {
        calories = inCalories;
    }
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * Set the description
     * @param inDescription The new description
     */
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    
    @Override
    public String toString()
    {
        return "Somebody brought " + getDescription();
    }
} 
}